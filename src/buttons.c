#include "buttons.h"

#include <logging/log.h>
#include <drivers/gpio.h>
#include <string.h>

LOG_MODULE_DECLARE(PerpiheralHrLR, CONFIG_LOG_MAX_LEVEL);

//configure buttons to simulate events
#define SW0_NODE 						DT_ALIAS(sw0)
#if DT_NODE_HAS_STATUS(SW0_NODE, okay)
#define SW0_GPIO_LABEL 					DT_GPIO_LABEL(SW0_NODE, gpios)
#define SW0_GPIO_PIN					DT_GPIO_PIN(SW0_NODE, gpios)
#define SW0_GPIO_FLAGS					(GPIO_INPUT | DT_GPIO_FLAGS(SW0_NODE, gpios))
#else
#define SW0_GPIO_LABEL 					''
#define SW0_GPIO_PIN					0
#define SW0_GPIO_FLAGS					0
#endif

#define SW1_NODE 						DT_ALIAS(sw1)
#if DT_NODE_HAS_STATUS(SW1_NODE, okay)
#define SW1_GPIO_LABEL 					DT_GPIO_LABEL(SW1_NODE, gpios)
#define SW1_GPIO_PIN					DT_GPIO_PIN(SW1_NODE, gpios)
#define SW1_GPIO_FLAGS					(GPIO_INPUT | DT_GPIO_FLAGS(SW1_NODE, gpios))
#else
#define SW1_GPIO_LABEL 					''
#define SW1_GPIO_PIN					0
#define SW1_GPIO_FLAGS					0
#endif

#define SW2_NODE 						DT_ALIAS(sw2)
#if DT_NODE_HAS_STATUS(SW2_NODE, okay)
#define SW2_GPIO_LABEL 					DT_GPIO_LABEL(SW2_NODE, gpios)
#define SW2_GPIO_PIN					DT_GPIO_PIN(SW2_NODE, gpios)
#define SW2_GPIO_FLAGS					(GPIO_INPUT | DT_GPIO_FLAGS(SW2_NODE, gpios))
#else
#define SW2_GPIO_LABEL 					''
#define SW2_GPIO_PIN					0
#define SW2_GPIO_FLAGS					0
#endif

#define SW3_NODE 						DT_ALIAS(sw3)
#if DT_NODE_HAS_STATUS(SW3_NODE, okay)
#define SW3_GPIO_LABEL 					DT_GPIO_LABEL(SW3_NODE, gpios)
#define SW3_GPIO_PIN					DT_GPIO_PIN(SW3_NODE, gpios)
#define SW3_GPIO_FLAGS					(GPIO_INPUT | DT_GPIO_FLAGS(SW3_NODE, gpios))
#else
#define SW3_GPIO_LABEL 					''
#define SW3_GPIO_PIN					0
#define SW3_GPIO_FLAGS					0
#endif

static struct gpio_callback button_cb_data1;
//static struct gpio_callback button_cb_data2;
//static struct gpio_callback button_cb_data3;
//static struct gpio_callback button_cb_data4;

#define MY_STACK_SIZE 512
#define MY_PRIORITY 5

void ble_disco(void);

// container for function operating in worker thread to handle button presses
struct button_info {
    struct k_work work;
    char name[16];
    uint8_t button_pressed;
} my_button;

//setup workqueue for when button is pressed
K_THREAD_STACK_DEFINE(my_button_stack, MY_STACK_SIZE);
static struct k_work_q my_work_q;

//local functions
static int configure_buttons(void);
static void button_press_worker(struct k_work *item);

//initalize buttons
int init_buttons(void) {
    //setup workqueue to offload button interrupts
    k_work_q_start(&my_work_q, my_button_stack, MY_STACK_SIZE, MY_PRIORITY);
    //assign callback to perform work
    k_work_init(&my_button.work, button_press_worker);
    //setup the buttons to simulate interrupts coming from the NFC chip
    return configure_buttons();
}

static void button_press_worker(struct k_work *item) {
    struct button_info *button = CONTAINER_OF(item, struct button_info, work);
    LOG_DBG("\n");
    LOG_DBG("%s press", log_strdup(button->name));

    //perform action based on which button was pressed
    if (button->button_pressed == 1) {
        ble_disco();
    } else if (button->button_pressed == 2) {
    } else if (button->button_pressed == 3) {
    } else {
    }
}

//callback for button 1 press
void button1_pressed_cb(const struct device *dev, struct gpio_callback *cb,
            uint32_t pins) {
    memcpy(my_button.name, "Button 1", 8);
    my_button.button_pressed = 1;
    //offload to lower priority thread
    k_work_submit(&my_button.work);
}

//callback for button 2 press
void button2_pressed_cb(const struct device *dev, struct gpio_callback *cb,
            uint32_t pins) {
    memcpy(my_button.name, "Button 2", 8);
    my_button.button_pressed = 2;
    //offload to lower priority thread
    k_work_submit(&my_button.work);
}

//callback for button 3 press
void button3_pressed_cb(const struct device *dev, struct gpio_callback *cb,
            uint32_t pins) {
    memcpy(my_button.name, "Button 3", 8);
    my_button.button_pressed = 3;
    //offload to lower priority thread
    k_work_submit(&my_button.work);
}

//callback for button 4 press
void button4_pressed_cb(const struct device *dev, struct gpio_callback *cb,
            uint32_t pins) {
    memcpy(my_button.name, "Button 4", 8);
    my_button.button_pressed = 4;
    //offload to lower priority thread
    k_work_submit(&my_button.work);
}

//helper to setup 4 buttons
static int configure_buttons(void) {
    const struct device *button = device_get_binding(SW0_GPIO_LABEL);
	if (button != NULL) {
        gpio_pin_configure(button, SW0_GPIO_PIN, SW0_GPIO_FLAGS);
        gpio_pin_interrupt_configure(button, SW0_GPIO_PIN, GPIO_INT_EDGE_TO_ACTIVE);
        gpio_init_callback(&button_cb_data1, button1_pressed_cb, BIT(SW0_GPIO_PIN));
        gpio_add_callback(button, &button_cb_data1);
	} else {
        return -1;
    }
#if 0
    button = device_get_binding(SW1_GPIO_LABEL);
    if (button != NULL) {
        gpio_pin_configure(button, SW1_GPIO_PIN, SW1_GPIO_FLAGS);
        gpio_pin_interrupt_configure(button, SW1_GPIO_PIN, GPIO_INT_EDGE_TO_ACTIVE);
                gpio_init_callback(&button_cb_data2, button2_pressed_cb, BIT(SW1_GPIO_PIN));
                gpio_add_callback(button, &button_cb_data2);
    } else {
        return -1;
    }
    button = device_get_binding(SW2_GPIO_LABEL);
    if (button != NULL) {
        gpio_pin_configure(button, SW2_GPIO_PIN, SW2_GPIO_FLAGS);
        gpio_pin_interrupt_configure(button, SW2_GPIO_PIN, GPIO_INT_EDGE_TO_ACTIVE);
        gpio_init_callback(&button_cb_data3, button3_pressed_cb, BIT(SW2_GPIO_PIN));
        gpio_add_callback(button, &button_cb_data3);
    } else {
        return -1;
    }
    button = device_get_binding(SW3_GPIO_LABEL);
    if (button != NULL) {
        gpio_pin_configure(button, SW3_GPIO_PIN, SW3_GPIO_FLAGS);
        gpio_pin_interrupt_configure(button, SW3_GPIO_PIN, GPIO_INT_EDGE_TO_ACTIVE);
        gpio_init_callback(&button_cb_data4, button4_pressed_cb, BIT(SW3_GPIO_PIN));
        gpio_add_callback(button, &button_cb_data4);
    } else {
        return -1;
    }
#endif
    return 0;
}