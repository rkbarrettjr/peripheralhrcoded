/*
 * Copyright (c) 2020 Nordic Semiconductor ASA
 *
 * SPDX-License-Identifier: LicenseRef-Nordic-5-Clause
 */

/** @file
 *  @brief Peripheral Heart Rate over LE Coded PHY sample
 */

#include <zephyr/types.h>
#include <stddef.h>
#include <string.h>
#include <errno.h>
#include <zephyr.h>
#include <logging/log.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/conn.h>
#include <bluetooth/uuid.h>
#include <bluetooth/gatt.h>
#include <bluetooth/services/bas.h>
#include <bluetooth/services/hrs.h>

#include <bluetooth/hci_vs.h>
#include <sys/byteorder.h>

#include "buttons.h"

static struct bt_conn *m_default_conn = NULL;

/** @brief Register logger for Fall Monitor */
LOG_MODULE_REGISTER(PerpiheralHrLR, CONFIG_LOG_MAX_LEVEL);
#define sd(x)   log_strdup((x))

static struct k_work start_advertising_worker;

static struct bt_le_ext_adv *adv;

static const struct bt_data ad[] = {
	BT_DATA_BYTES(BT_DATA_FLAGS, (BT_LE_AD_GENERAL | BT_LE_AD_NO_BREDR)),
	BT_DATA_BYTES(BT_DATA_UUID16_ALL, 0x0d, 0x18, 0x0f, 0x18, 0x0a, 0x18),
};

static void connected(struct bt_conn *conn, uint8_t conn_err)
{
	int err;
	struct bt_conn_info info;
	char addr[BT_ADDR_LE_STR_LEN];

	bt_addr_le_to_str(bt_conn_get_dst(conn), addr, sizeof(addr));

	if (conn_err) {
		LOG_ERR("Connection failed (err %d)\n", conn_err);
		return;
	}

        err = bt_conn_le_param_update(conn, BT_LE_CONN_PARAM(12, 14, 4, 225));
        if (err) {
            LOG_ERR("Parameter update rejected by Central");
        }

	err = bt_conn_get_info(conn, &info);
        m_default_conn = bt_conn_ref(conn);

	if (err) {
		LOG_ERR("Failed to get connection info\n");
	} else {
		const struct bt_conn_le_phy_info *phy_info;
		phy_info = info.le.phy;

		LOG_DBG("Connected: %s, tx_phy %u, rx_phy %u",
		       sd(addr), phy_info->tx_phy, phy_info->rx_phy);
	}
}

static void disconnected(struct bt_conn *conn, uint8_t reason)
{
	LOG_DBG("Disconnected (reason 0x%02x)", reason);

	k_work_submit(&start_advertising_worker);
}

static struct bt_conn_cb conn_callbacks = {
	.connected = connected,
	.disconnected = disconnected,
};

static int create_advertising_coded(void)
{
	int err;
	struct bt_le_adv_param param =
		BT_LE_ADV_PARAM_INIT(BT_LE_ADV_OPT_CONNECTABLE |
				     BT_LE_ADV_OPT_EXT_ADV |
				     BT_LE_ADV_OPT_CODED,
				     BT_GAP_ADV_FAST_INT_MIN_2,
				     BT_GAP_ADV_FAST_INT_MAX_2,
				     NULL);

	err = bt_le_ext_adv_create(&param, NULL, &adv);
	if (err) {
		LOG_ERR("Failed to create advertiser set (%d)\n", err);
		return err;
	}

	LOG_DBG("Created adv: %p", adv);

	err = bt_le_ext_adv_set_data(adv, ad, ARRAY_SIZE(ad), NULL, 0);
	if (err) {
		LOG_ERR("Failed to set advertising data (%d)\n", err);
		return err;
	}

	return 0;
}

static void start_advertising_coded(struct k_work *item)
{
	int err;

	err = bt_le_ext_adv_start(adv, NULL);
	if (err) {
		LOG_ERR("Failed to start advertising set (%d)\n", err);
		return;
	}

	LOG_DBG("Advertiser %p set started", adv);
}

static void bt_ready(void)
{
	int err = 0;

	LOG_DBG("Bluetooth initialized");

	k_work_init(&start_advertising_worker, start_advertising_coded);

	err = create_advertising_coded();
	if (err) {
		LOG_ERR("Advertising failed to create (err %d)\n", err);
		return;
	}

	k_work_submit(&start_advertising_worker);
}

static void bas_notify(void)
{
	uint8_t battery_level = bt_bas_get_battery_level();

	battery_level--;

	if (!battery_level) {
		battery_level = 100U;
	}

	bt_bas_set_battery_level(battery_level);
}

static void hrs_notify(void)
{
	static uint8_t heartrate = 90U;

	/* Heartrate measurements simulation */
	heartrate++;
	if (heartrate == 160U) {
		heartrate = 90U;
	}

	bt_hrs_notify(heartrate);
}

static void get_tx_power(int8_t *tx_pwr_lvl, uint8_t handle_type) {
    struct bt_hci_cp_vs_read_tx_power_level *cp;
    struct bt_hci_rp_vs_read_tx_power_level *rp;
    struct net_buf *buf, *rsp = NULL;
    uint16_t handle = 0;
    int err;

    *tx_pwr_lvl = 0xFF;
    buf = bt_hci_cmd_create(BT_HCI_OP_VS_READ_TX_POWER_LEVEL, sizeof(*cp));
    if (!buf) {
        LOG_ERR("Unable to allocate command buffer");
        return;
    }

    cp = net_buf_add(buf, sizeof(*cp));
    cp->handle = sys_cpu_to_le16(handle);
    cp->handle_type = handle_type;

    err = bt_hci_cmd_send_sync(BT_HCI_OP_VS_READ_TX_POWER_LEVEL, buf, &rsp);
    if (err) {
        uint8_t reason = rsp ? ((struct bt_hci_rp_vs_read_tx_power_level *)rsp->data)->status : 0;
        LOG_ERR("Read Tx power err: %d reason 0x%02x", err, reason);
        return;
    }

    rp = (void *)rsp->data;
    *tx_pwr_lvl = rp->tx_power_level;

    net_buf_unref(rsp);
}

static int set_tx_power(int8_t tx_pwr_lvl, uint8_t handle_type) {
    struct bt_hci_cp_vs_write_tx_power_level *cp;
    struct bt_hci_rp_vs_write_tx_power_level *rp;
    struct net_buf *buf, *rsp = NULL;
    uint16_t handle = 0;
    int err;

    int8_t default_power;
    int8_t updated_power;
    get_tx_power(&default_power, handle_type);
   
    buf = bt_hci_cmd_create(BT_HCI_OP_VS_WRITE_TX_POWER_LEVEL, sizeof(*cp));
    if (!buf) {
        LOG_ERR("Unable to allocate command buffer");
        return 1;
    }
   
    cp = net_buf_add(buf, sizeof(*cp));
    cp->handle = sys_cpu_to_le16(handle);
    cp->handle_type = handle_type;
    cp->tx_power_level = tx_pwr_lvl;
   
    err = bt_hci_cmd_send_sync(BT_HCI_OP_VS_WRITE_TX_POWER_LEVEL, buf, &rsp);
    if (err) {
        uint8_t reason = rsp ?
            ((struct bt_hci_rp_vs_write_tx_power_level *)
              rsp->data)->status : 0;
        LOG_ERR("Set Tx power err: %d reason 0x%02x", err, reason);
        return err;
    }
   
    rp = (void *)rsp->data;
    LOG_DBG("Response BLE Tx Power: %d", rp->selected_tx_power);

    get_tx_power(&updated_power, handle_type);

    if (handle_type == BT_HCI_VS_LL_HANDLE_TYPE_ADV) {
        LOG_DBG("Advertising power modified : %d -> %d", default_power, updated_power);
    } else if (handle_type == BT_HCI_VS_LL_HANDLE_TYPE_CONN) {
        LOG_DBG("Connection power modified  : %d -> %d", default_power, updated_power);
    }

    net_buf_unref(rsp);
    return 0;
}

void ble_disco(void) {
    bt_conn_disconnect(m_default_conn, BT_HCI_ERR_REMOTE_USER_TERM_CONN);
    bt_conn_unref(m_default_conn);
    m_default_conn = NULL;
}

void main(void)
{
	int err;

        init_buttons();

	err = bt_enable(NULL);
	if (err) {
		LOG_ERR("Bluetooth init failed (err %d)\n", err);
		return;
	}

	bt_ready();
        k_sleep(K_SECONDS(1));


        int8_t power_lvl = 8;
        uint8_t handle_type = BT_HCI_VS_LL_HANDLE_TYPE_ADV;
        err = set_tx_power(power_lvl, handle_type);
        if (err) {
            LOG_ERR("Error setting Advertising TX power\n");
        } else {
            LOG_DBG("Power set");
        }

        k_sleep(K_SECONDS(1));

        handle_type = BT_HCI_VS_LL_HANDLE_TYPE_CONN;
        err = set_tx_power(power_lvl, handle_type);
        if (err) {
            LOG_ERR("Error setting Connection TX power\n");
        } else {
            LOG_DBG("Power set");
        }

	bt_conn_cb_register(&conn_callbacks);

	/* Implement notification. At the moment there is no suitable way
	 * of starting delayed work so we do it here
	 */
	while (1) {
		k_sleep(K_SECONDS(1));

		/* Heartrate measurements simulation */
		hrs_notify();

		/* Battery level simulation */
		bas_notify();
	}
}
